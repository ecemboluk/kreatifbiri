package app.kreatifbiri.com.kreatifbiricom.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentPost {

    @SerializedName("author_name")
    @Expose
    private String authorName;
    @SerializedName("author_email")
    @Expose
    private String authorEmail;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("post")
    @Expose
    private String postId;

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }
}
