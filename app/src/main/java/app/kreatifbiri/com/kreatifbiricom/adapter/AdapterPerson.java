package app.kreatifbiri.com.kreatifbiricom.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.model.Person;

public class AdapterPerson extends RecyclerView.Adapter<AdapterPerson.MyViewHolder> {

    private List<Person> personList;
    private Context context;
    private Typeface typeface;

    public AdapterPerson(List<Person> personList, Context context){
        this.personList=personList;
        this.context=context;
    }

    @Override
    public AdapterPerson.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_persons, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(AdapterPerson.MyViewHolder holder, int position) {
        Person person=personList.get(position);
        typeface = Typeface.createFromAsset(context.getAssets(), "fonts/Raleway-Thin.ttf");
        holder.name.setTypeface(typeface);
        holder.task.setTypeface(typeface);
        holder.name.setText(person.getName());
        holder.task.setText(person.getTask());
        if(person.getImageURL()!=""){
            Glide.with(context).load(person.getImageURL()).into(holder.image);
        }
        else{
            holder.image.setImageResource(R.drawable.kreatifbiricom);
        }

    }

    @Override
    public int getItemCount() {
        return personList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        ImageView image;
        TextView name;
        TextView task;

        public MyViewHolder(View itemView) {
            super(itemView);
            image=itemView.findViewById(R.id.imageperson);
            name=itemView.findViewById(R.id.personame);
            task=itemView.findViewById(R.id.task);
        }
    }
}
