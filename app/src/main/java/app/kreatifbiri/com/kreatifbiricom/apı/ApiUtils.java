package app.kreatifbiri.com.kreatifbiricom.apı;

import app.kreatifbiri.com.kreatifbiricom.retrofit.RetrofitClient;
import app.kreatifbiri.com.kreatifbiricom.ınterface.WordpressServices;

/**
 * Created by ecem on 1.05.2018.
 */

public class ApiUtils {
    public static final String BASE_URL = "https://www.kreatifbiri.com/";

    public static WordpressServices getWordpressServices() {
        return RetrofitClient.getClient(BASE_URL).create(WordpressServices.class);
    }

}
