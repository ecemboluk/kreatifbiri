package app.kreatifbiri.com.kreatifbiricom.ınterface;

import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.model.CommentPost;
import app.kreatifbiri.com.kreatifbiricom.model.Post;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by PK on 2/4/2018.
 */
public interface WordpressServices {

    @GET("wp-json/wp/v2/posts")
    Call<List<Post>> getAllPost(@Query("page") String pagenumber,
                                @Query("per_page") String perpage,
                                @Query("_embed") String embed);


    @POST("wp-json/wp/v2/comments")
    Call<CommentPost> sendComment(@Query("author_name") String authorName,
                                  @Query("author_email") String authorEmail,
                                  @Query("content") String content,
                                  @Query("post") String postId);
    @GET("wp-json/wp/v2/posts")
    Call<List<Post>> categoryDusunen(@Query("page") String pagenumber,
                               @Query("per_page") String perpage,
                               @Query("categories") String categories,
                               @Query("_embed") String embed);

    @GET("wp-json/wp/v2/posts")
    Call<List<Post>> categoryKulturlu(@Query("page") String pagenumber,
                                     @Query("per_page") String perpage,
                                     @Query("categories") String categories,
                                     @Query("_embed") String embed);
    @GET("wp-json/wp/v2/posts")
    Call<List<Post>> categorySosyal(@Query("page") String pagenumber,
                                      @Query("per_page") String perpage,
                                      @Query("categories") String categories,
                                      @Query("_embed") String embed);

    @GET("wp-json/wp/v2/posts")
    Call<List<Post>> categoryBilimsel(@Query("page") String pagenumber,
                                    @Query("per_page") String perpage,
                                    @Query("categories") String categories,
                                    @Query("_embed") String embed);
}