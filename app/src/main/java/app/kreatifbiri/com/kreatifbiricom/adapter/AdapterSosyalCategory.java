package app.kreatifbiri.com.kreatifbiricom.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.activity.ContentActivity;
import app.kreatifbiri.com.kreatifbiricom.model.Author_;
import app.kreatifbiri.com.kreatifbiricom.model.Post;
import app.kreatifbiri.com.kreatifbiricom.model.WpFeaturedmedium_;
import app.kreatifbiri.com.kreatifbiricom.model.WpTerm_;

public class AdapterSosyalCategory extends RecyclerView.Adapter<AdapterSosyalCategory.MyAdapter>{

    private List<Post> postsList;
    private List<String> yazarList=new ArrayList<String>();
    private List<String> yorumList=new ArrayList<String>();
    private List<String> imageURL=new ArrayList<String>();
    private List<WpTerm_> wpTerm;
    private Activity activity;
    private Post posts;
    private WpFeaturedmedium_ wpFeaturedmedium;
    private Author_ author;
    private Typeface typeface;

    public AdapterSosyalCategory(List<Post> posts, Activity activity){
        this.activity=activity;
        this.postsList=posts;
    }

    @Override
    public AdapterSosyalCategory.MyAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=null;
        MyAdapter myAdapter = null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_mainpage, parent, false);
        myAdapter=new MyAdapter(view);
        return myAdapter;
    }

    @Override
    public void onBindViewHolder(AdapterSosyalCategory.MyAdapter holder, final int position) {
            posts=postsList.get(position);
            wpTerm=postsList.get(position).getEmbedded().getWpTerm().get(0);
            wpFeaturedmedium=postsList.get(position).getEmbedded().getWpFeaturedmedia().get(0);
            author=postsList.get(position).getEmbedded().getAuthor().get(0);
            typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/Raleway-Thin.ttf");
            holder.contentName.setTypeface(typeface);
            holder.contentName.setText(posts.getTitle().getRendered().replace("&#8217;","'").replace("&#8220;","'")
                    .replace("&#8221;","'"));
            final String cat=wpTerm.get(0).getName();
            holder.kategori.setTypeface(typeface);
            holder.kategori.setText(wpTerm.get(0).getName());
            Glide.with(activity).load(wpFeaturedmedium.getSourceUrl()).into(holder.contentImage);
            holder.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent ıntent=new Intent(activity, ContentActivity.class);
                    if(postsList.get(position).getEmbedded().getReplies()==null){
                        ıntent.putExtra("yorum","no");
                    }
                    else{
                        ıntent.putExtra("yorum","yes");
                        int size=postsList.get(position).getEmbedded().getReplies().get(0).size();
                        for(int i=size-1; i>=0; i--){
                            yazarList.add(postsList.get(position).getEmbedded().getReplies().get(0).get(i).getAuthorName());
                            yorumList.add(postsList.get(position).getEmbedded().getReplies().get(0).get(i).getContent().getRendered());
                            imageURL.add(postsList.get(position).getEmbedded().getReplies().get(0).get(i).getAvatarUrls().get24());
                        }
                        ıntent.putStringArrayListExtra("commentsA", (ArrayList<String>) yazarList);
                        ıntent.putStringArrayListExtra("comments", (ArrayList<String>) yorumList);
                        ıntent.putStringArrayListExtra("images", (ArrayList<String>) imageURL);
                    }
                    ıntent.putExtra("kategori",cat);
                    ıntent.putExtra("image",postsList.get(position).getEmbedded().getWpFeaturedmedia().get(0).getSourceUrl());
                    ıntent.putExtra("content",postsList.get(position).getContent().getRendered());
                    ıntent.putExtra("yazar",postsList.get(position).getEmbedded().getAuthor().get(0).getName());
                    ıntent.putExtra("desc",postsList.get(position).getEmbedded().getAuthor().get(0).getDescription());
                    ıntent.putExtra("yazarPhoto",postsList.get(position).getEmbedded().getAuthor().get(0).getAvatarUrls().get96());
                    ıntent.putExtra("id",postsList.get(position).getId());
                    ıntent.putExtra("title",postsList.get(position).getTitle().getRendered().replace("&#8217;","'"));
                    activity.startActivity(ıntent);
                }
            });
    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }
    public void add(List<Post> posts){
        int size=postsList.size()-1;
        postsList.addAll(posts);
        notifyItemRangeInserted(size, postsList.size());
    }
    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public class MyAdapter extends RecyclerView.ViewHolder {

        CardView cv;
        TextView contentName;
        TextView kategori;
        ImageView contentImage;
        RecyclerView recyclerView;

        public MyAdapter(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cvMainpage);
            contentName = (TextView)itemView.findViewById(R.id.icerik_adi);
            kategori = (TextView) itemView.findViewById(R.id.cat);
            contentImage=(ImageView)itemView.findViewById(R.id.icerik_foto);
            recyclerView=(RecyclerView)itemView.findViewById(R.id.rBilimsel);
        }
    }
}
