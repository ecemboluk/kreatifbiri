package app.kreatifbiri.com.kreatifbiricom.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;

import com.github.pwittchen.infinitescroll.library.InfiniteScrollListener;

import java.util.ArrayList;
import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.adapter.AdapterBilimselCategory;
import app.kreatifbiri.com.kreatifbiricom.adapter.AdapterKulturelCategory;
import app.kreatifbiri.com.kreatifbiricom.apı.ApiUtils;
import app.kreatifbiri.com.kreatifbiricom.model.ListData;
import app.kreatifbiri.com.kreatifbiricom.model.Post;
import app.kreatifbiri.com.kreatifbiricom.ınterface.WordpressServices;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class KulturelBiriActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.rKulturel)
    RecyclerView recyclerView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private Activity activity=this;
    private AdapterKulturelCategory adapterKulturelCategory;
    private WordpressServices wordpressServices;
    private List<Post> postList;
    private  LinearLayoutManager llm;
    private int page=1,previousTotal=0,visibleThreshold=10;
    private boolean loading=true;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kulturel_biri);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        wordpressServices = ApiUtils.getWordpressServices();
        scrollReycleView();
    }

    private void scrollReycleView(){
        adapterKulturelCategory = new AdapterKulturelCategory(ListData.kulturelList, activity);
        llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapterKulturelCategory);
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = recyclerView.getChildCount();
                int totalItemCount = llm.getItemCount();
                int firstVisibleItem = llm.findFirstVisibleItemPosition();
                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    page++;
                    load(page);
                    loading = true;
                }
                else{
                    page++;
                    load(page);
                    loading = true;
                }
            }
        });
    }
    private void load(int page) {
        wordpressServices.categoryKulturlu(String.valueOf(page),"10","115","true").enqueue(new Callback<List<Post>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    postList=response.body();
                    adapterKulturelCategory.add(postList);
                    adapterKulturelCategory.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent ıntent=new Intent(this,MainPageActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_backin,R.anim.anim_backout);
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id==R.id.kreatifbiri){
            Intent ıntent=new Intent(this,MainPageActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in,R.anim.anim_out);
        }
        else if (id == R.id.whoare) {
            Intent ıntent=new Intent(this,BizKimizActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.whatsproblem) {
            Intent ıntent=new Intent(this,DerdimizNeActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.bilimselbiri) {
            Intent ıntent=new Intent(this,BilimselBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.sosyalbiri) {
            Intent ıntent=new Intent(this,SosyalBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.dusunenbiri) {
            Intent ıntent=new Intent(this,DusunenBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        else if (id == R.id.com) {
            Intent ıntent=new Intent(this,İletisimActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        else if (id == R.id.team) {
            Intent ıntent=new Intent(this,EkipActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        else if(id==R.id.facebook){
            Uri webpage=Uri.parse("https://www.facebook.com/kreatif6/");
            Intent webIntent=new Intent(Intent.ACTION_VIEW,webpage);
            this.startActivity(webIntent);
        }
        else if(id==R.id.instagram){
            Uri webpage=Uri.parse("https://www.instagram.com/kreatifbiri/");
            Intent webIntent=new Intent(Intent.ACTION_VIEW,webpage);
            this.startActivity(webIntent);
        }
        else if(id==R.id.twitter){
            Uri webpage = Uri.parse("https://twitter.com/kreatifbiri");
            Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
            this.startActivity(webIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
