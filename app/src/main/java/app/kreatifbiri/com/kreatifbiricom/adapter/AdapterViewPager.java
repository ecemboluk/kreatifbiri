package app.kreatifbiri.com.kreatifbiricom.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.activity.ContentActivity;
import app.kreatifbiri.com.kreatifbiricom.model.Post;
import app.kreatifbiri.com.kreatifbiricom.model.WpTerm_;

public class AdapterViewPager extends PagerAdapter{

    private Context context;
    private LayoutInflater layoutInflater;
    private List<Post> postList;
    private Post post;
    private List<String> yazarList=new ArrayList<String>();
    private List<String> yorumList=new ArrayList<String>();
    private List<String> imageURL=new ArrayList<String>();

    public AdapterViewPager(Context context, List<Post> posts){
        this.context = context;
        this.postList = posts;
    }

    @Override
    public int getCount() {
        return postList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position) {
        post=postList.get(position);
        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(R.layout.custom_layout, null);
        ImageView imageView = (ImageView) view.findViewById(R.id.imageView4);
        TextView title=(TextView)view.findViewById(R.id.titleF);
        Glide.with(context).load(post.getEmbedded().getWpFeaturedmedia().get(0).getSourceUrl()).into(imageView);
        title.setText(post.getTitle().getRendered().replace("&#8217;","'")
                .replace("&#8220;","'").replace("&#8221;","'"));
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ıntent=new Intent(context, ContentActivity.class);
                if(postList.get(position).getEmbedded().getReplies()==null){
                    ıntent.putExtra("yorum","no");
                }
                else{
                    ıntent.putExtra("yorum","yes");
                    int size=postList.get(position).getEmbedded().getReplies().get(0).size();
                    for(int i=size-1; i>=0; i--){
                        yazarList.add(postList.get(position).getEmbedded().getReplies().get(0).get(i).getAuthorName());
                        yorumList.add(postList.get(position).getEmbedded().getReplies().get(0).get(i).getContent().getRendered());
                        imageURL.add(postList.get(position).getEmbedded().getReplies().get(0).get(i).getAvatarUrls().get96());
                    }
                    ıntent.putStringArrayListExtra("commentsA", (ArrayList<String>) yazarList);
                    ıntent.putStringArrayListExtra("comments", (ArrayList<String>) yorumList);
                    ıntent.putStringArrayListExtra("images", (ArrayList<String>) imageURL);
                }
                ıntent.putExtra("kategori",postList.get(position).getEmbedded().getWpTerm().get(0).get(0).getName());
                ıntent.putExtra("image",postList.get(position).getEmbedded().getWpFeaturedmedia().get(0).getSourceUrl());
                ıntent.putExtra("content",postList.get(position).getContent().getRendered());
                ıntent.putExtra("yazar",postList.get(position).getEmbedded().getAuthor().get(0).getName());
                ıntent.putExtra("desc",postList.get(position).getEmbedded().getAuthor().get(0).getDescription());
                ıntent.putExtra("yazarPhoto",postList.get(position).getEmbedded().getAuthor().get(0).getAvatarUrls().get96());
                ıntent.putExtra("id",postList.get(position).getId());
                ıntent.putExtra("title",postList.get(position).getTitle().getRendered().replace("&#8217;","'"));
                context.startActivity(ıntent);
            }
        });


        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);
        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}
