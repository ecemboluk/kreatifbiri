package app.kreatifbiri.com.kreatifbiricom.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.apı.GMailSender;
import butterknife.BindView;
import butterknife.ButterKnife;

public class İletisimActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.subject)
    EditText subject;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.contentC)
    EditText content;
    @BindView(R.id.iletisimText)
    TextView iletisimText;

    private Typeface typeface;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iletisim);
        setSupportActionBar(toolbar);
        ButterKnife.bind(this);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Thin.ttf");
        iletisimText.setTypeface(typeface);
    }

    public void onSendSubject(View v){
        if(subject.getText().toString()!="" && name.getText().toString()!="" &&email.getText().toString()!=""&&
        content.getText().toString()!=""){
           new SendMailTask().execute();
        }
        else{
            Toast.makeText(this,"Lütfen boş bıraktığınız yerleri doldurunuz.",Toast.LENGTH_LONG).show();
        }
    }


    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent ıntent=new Intent(this,MainPageActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_backin,R.anim.anim_backout);
            super.onBackPressed();
        }
    }
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id==R.id.kreatifbiri){
            Intent ıntent=new Intent(this,MainPageActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in,R.anim.anim_out);
        }
        else if (id == R.id.whoare) {
            Intent ıntent=new Intent(this,BizKimizActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.whatsproblem) {
            Intent ıntent=new Intent(this,DerdimizNeActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.bilimselbiri) {
            Intent ıntent=new Intent(this,BilimselBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.sosyalbiri) {
            Intent ıntent=new Intent(this,SosyalBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.kulturelbiri) {
            Intent ıntent=new Intent(this,KulturelBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.dusunenbiri) {
            Intent ıntent=new Intent(this,DusunenBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        else if (id == R.id.team) {
            Intent ıntent=new Intent(this,EkipActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        else if(id==R.id.facebook){
            Uri webpage=Uri.parse("https://www.facebook.com/kreatif6/");
            Intent webIntent=new Intent(Intent.ACTION_VIEW,webpage);
            this.startActivity(webIntent);
        }
        else if(id==R.id.instagram){
            Uri webpage=Uri.parse("https://www.instagram.com/kreatifbiri/");
            Intent webIntent=new Intent(Intent.ACTION_VIEW,webpage);
            this.startActivity(webIntent);
        }
        else if(id==R.id.twitter){
            Uri webpage = Uri.parse("https://twitter.com/kreatifbiri");
            Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
            this.startActivity(webIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class SendMailTask extends AsyncTask<Void,Void,Void>{

        ProgressDialog dialog;//dialog nesnesini tanımladık
        @Override
        protected  void onPreExecute()//Verilerin çekilme esnasında proggres dialog çalıştırır.
        {
            super.onPreExecute();
            dialog=new ProgressDialog(İletisimActivity.this);
            dialog.setMessage("Mesajınız Gönderiliyor...");
            dialog.setIndeterminate(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                GMailSender sender = new GMailSender("kreatifbiri@gmail.com",
                        "75svrz11");
                sender.sendMail(subject.getText().toString(), "Ad Soyad: "+name.getText().toString()+"\n"+"eposta :"+email.getText().toString()
                        +"\n"+content.getText().toString(),
                        "kreatifbiri@gmail.com", "kreatifbiri@gmail.com");
            } catch (Exception e) {

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void avoid)//Arka plan işlemleri bittikten sonra başlık yazdırılır.
        {
            dialog.dismiss();
        }
    }
}
