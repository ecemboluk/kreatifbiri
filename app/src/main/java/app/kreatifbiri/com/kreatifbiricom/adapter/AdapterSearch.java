package app.kreatifbiri.com.kreatifbiricom.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.activity.ContentActivity;
import app.kreatifbiri.com.kreatifbiricom.model.Author_;
import app.kreatifbiri.com.kreatifbiricom.model.Post;
import app.kreatifbiri.com.kreatifbiricom.model.WpFeaturedmedium_;
import app.kreatifbiri.com.kreatifbiricom.model.WpTerm_;

public class AdapterSearch extends RecyclerView.Adapter<AdapterSearch.MyAdapter>  {

    private List<Post> postList,filteredPostList;
    private List<WpTerm_> wpTerm;
    private List<String> yazarList=new ArrayList<String>();
    private List<String> yorumList=new ArrayList<String>();
    private List<String> imageURL=new ArrayList<String>();
    private Activity activity;
    private WpFeaturedmedium_ wpFeaturedmedium;
    private Author_ author;
    private Typeface typeface;
    public AdapterSearch(List<Post> posts, Activity activity){
        this.postList=posts;
        this.filteredPostList=posts;
        this.activity=activity;
    }

    @Override
    public AdapterSearch.MyAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=null;
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_mainpage, parent, false);
        return new MyAdapter(view);
    }

    @Override
    public void onBindViewHolder(MyAdapter holder, final int position) {
        wpTerm=filteredPostList.get(position).getEmbedded().getWpTerm().get(0);
        wpFeaturedmedium=filteredPostList.get(position).getEmbedded().getWpFeaturedmedia().get(0);
        author=filteredPostList.get(position).getEmbedded().getAuthor().get(0);
        typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/Raleway-Thin.ttf");
        holder.contentName.setTypeface(typeface);
        holder.contentName.setText(filteredPostList.get(position).getTitle().getRendered().replace("&#8217;","'")
        .replace("&#8220;","'").replace("&#8221;","'"));
        holder.kategori.setTypeface(typeface);
        holder.kategori.setText(wpTerm.get(0).getName());
        final String cat=wpTerm.get(0).getName();
        Glide.with(activity).load(wpFeaturedmedium.getSourceUrl()).into(holder.contentImage);
        holder.cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent ıntent=new Intent(activity, ContentActivity.class);
                if(filteredPostList.get(position).getEmbedded().getReplies()==null){
                    ıntent.putExtra("yorum","no");
                }
                else{
                    ıntent.putExtra("yorum","yes");
                    int size=filteredPostList.get(position).getEmbedded().getReplies().get(0).size();
                    for(int i=size-1; i>=0; i--){
                        yazarList.add(filteredPostList.get(position).getEmbedded().getReplies().get(0).get(i).getAuthorName());
                        yorumList.add(filteredPostList.get(position).getEmbedded().getReplies().get(0).get(i).getContent().getRendered());
                        imageURL.add(filteredPostList.get(position).getEmbedded().getReplies().get(0).get(i).getAvatarUrls().get96());
                    }
                    ıntent.putStringArrayListExtra("commentsA", (ArrayList<String>) yazarList);
                    ıntent.putStringArrayListExtra("comments", (ArrayList<String>) yorumList);
                    ıntent.putStringArrayListExtra("images", (ArrayList<String>) imageURL);
                }
                ıntent.putExtra("kategori",cat);
                ıntent.putExtra("image",filteredPostList.get(position).getEmbedded().getWpFeaturedmedia().get(0).getSourceUrl());
                ıntent.putExtra("content",filteredPostList.get(position).getContent().getRendered());
                ıntent.putExtra("yazar",filteredPostList.get(position).getEmbedded().getAuthor().get(0).getName());
                ıntent.putExtra("desc",filteredPostList.get(position).getEmbedded().getAuthor().get(0).getDescription());
                ıntent.putExtra("yazarPhoto",filteredPostList.get(position).getEmbedded().getAuthor().get(0).getAvatarUrls().get96());
                ıntent.putExtra("id",filteredPostList.get(position).getId());
                ıntent.putExtra("title",filteredPostList.get(position).getTitle().getRendered().replace("&#8217;","'"));
                activity.startActivity(ıntent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return filteredPostList.size();
    }
    public void filter(String query, List<Post> posts) {
        int size=filteredPostList.size()-1;
        postList.addAll(posts);
        //filteredPostList.addAll(posts);
        ArrayList<Post> tempFilteredList = new ArrayList<>();
        for (int i = 0; i < postList.size(); i++) {
            // search for user name
            if (postList.get(i).getTitle().getRendered().toLowerCase().contains(query.toLowerCase()) ||
                     postList.get(i).getEmbedded().getAuthor().get(0).getName().toLowerCase().contains(query.toLowerCase())
                    || postList.get(i).getEmbedded().getWpTerm().get(0).get(0).getName().toLowerCase().contains(query.toLowerCase())) {
                tempFilteredList.add(postList.get(i));
            }
        }
        filteredPostList = tempFilteredList;
        notifyItemRangeInserted(size, filteredPostList.size());
    }

    public static class MyAdapter extends RecyclerView.ViewHolder{

        CardView cv;
        TextView contentName;
        TextView kategori;
        ImageView contentImage;
        public MyAdapter(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cvMainpage);
            contentName = (TextView)itemView.findViewById(R.id.icerik_adi);
            kategori = (TextView) itemView.findViewById(R.id.cat);
            contentImage=(ImageView)itemView.findViewById(R.id.icerik_foto);
        }
    }
}
