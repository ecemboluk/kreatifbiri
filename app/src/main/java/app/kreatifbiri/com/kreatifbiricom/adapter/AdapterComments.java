package app.kreatifbiri.com.kreatifbiricom.adapter;

import android.app.Activity;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;
import org.jsoup.Jsoup;

import app.kreatifbiri.com.kreatifbiricom.R;

public class AdapterComments extends RecyclerView.Adapter<AdapterComments.ContentViewHolder> {

    private List<String> yorumList=new ArrayList<String>();
    private List<String>yazarList=new ArrayList<String>();
    private List<String> imageList=new ArrayList<String>();
    private String yazar,yorum;
    private Activity activity;
    private Typeface typeface;

    public AdapterComments(List<String> yorum, List<String>yazar, List<String> image,Activity activity){
        this.yorumList=yorum;
        this.yazarList=yazar;
        this.imageList=image;
        this.activity=activity;
        notifyDataSetChanged();
    }
    @Override
    public AdapterComments.ContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_comments, parent, false);
        return new ContentViewHolder(v);
    }

    @Override
    public void onBindViewHolder(AdapterComments.ContentViewHolder holder, int position) {
        yazar=yazarList.get(position);
        yorum=yorumList.get(position);
        yazar=Jsoup.parse(yazar).text();
        yorum=Jsoup.parse(yorum).text();
        typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/Raleway-Thin.ttf");
        holder.yazar.setTypeface(typeface);
        holder.yorum.setTypeface(typeface);
        holder.yazar.setText(yazar);
        holder.yorum.setText(yorum);
        Glide.with(activity).load(imageList.get(position)).into(holder.image);
    }

    @Override
    public int getItemCount() {
        return yazarList.size();
    }

    public class ContentViewHolder extends RecyclerView.ViewHolder  {
        CardView cv;
        TextView yazar;
        TextView yorum;
        ImageView image;

        ContentViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cvComments);
            yazar = (TextView)itemView.findViewById(R.id.name);
            yorum = (TextView) itemView.findViewById(R.id.comment);
            image=(ImageView)itemView.findViewById(R.id.images);
        }

    }
}
