package app.kreatifbiri.com.kreatifbiricom.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterViewFlipper;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;


import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.adapter.Adapter;
import app.kreatifbiri.com.kreatifbiricom.adapter.AdapterViewPager;
import app.kreatifbiri.com.kreatifbiricom.apı.ApiUtils;
import app.kreatifbiri.com.kreatifbiricom.model.ListData;
import app.kreatifbiri.com.kreatifbiricom.model.Post;
import app.kreatifbiri.com.kreatifbiricom.ınterface.WordpressServices;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainPageActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.rMainpage)
    RecyclerView rv;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;

    private Adapter adapter;
    private Context context=this;
    private WordpressServices wordpressServices;
    private List<Post> postList;
    private PullRefreshLayout layout;
    private int page=1,previousTotal=0,visibleThreshold=12;
    private LinearLayoutManager llm;
    private boolean loading = true;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        navigationView.setNavigationItemSelectedListener(this);
        wordpressServices= ApiUtils.getWordpressServices();
        postList= ListData.postList;
        adapter=new Adapter(postList,this);
        adapter.flipperAdd(postList);
        llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapter);
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = rv.getChildCount();
                int totalItemCount = llm.getItemCount();
                int firstVisibleItem = llm.findFirstVisibleItemPosition();
                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    page++;
                    load(page);
                    loading = true;
                }
                else{
                    page++;
                    load(page);
                    loading = true;
                }
            }
        });
        bilimsel();
        dusunen();
        kulturel();
        sosyal();
        search();
        OneSignal.startInit(this)
                .setNotificationOpenedHandler(new OneSignal.NotificationOpenedHandler() {
                    @Override
                    public void notificationOpened(OSNotificationOpenResult result) {
                        String launchURL = result.notification.payload.launchURL;
                        if(launchURL!=null){
                            Intent intent = new Intent(getApplicationContext(), MainPageActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        }
                    }
                })
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        layout=(PullRefreshLayout)findViewById(R.id.reflesh);
        layout.setColor(R.color.colorAccent);
        layout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                wordpressServices.getAllPost("1","10","true").enqueue(new Callback<List<Post>>() {
                    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
                    @Override
                    public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                        if(response.isSuccessful()){
                            ListData.postList=response.body();
                            adapter.add(ListData.postList);
                            adapter.notifyDataSetChanged();
                        }
                        layout.setRefreshing(false);
                    }
                    @Override
                    public void onFailure(Call<List<Post>> call, Throwable t) {

                    }
                });
            }
        });
    }
    private void load(int page) {
        wordpressServices.getAllPost(String.valueOf(page),"10","true").enqueue(new Callback<List<Post>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    postList=response.body();
                    adapter.add(postList);
                    adapter.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }

    public boolean InternetKontrol() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager.getActiveNetworkInfo() != null && manager.getActiveNetworkInfo().isAvailable()
                && manager.getActiveNetworkInfo().isConnected()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_item, menu);

        MenuItem searchItem = menu.findItem(R.id.menu_search);

        SearchView searchView = null;
        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
        }
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent intent=new Intent(context,SearchActivity.class);
                intent.putExtra("query",query);
                context.startActivity(intent);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                return true;
            }
        });

        return super.onCreateOptionsMenu(menu);
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.whoare) {
            Intent ıntent=new Intent(this,BizKimizActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.whatsproblem) {
            Intent ıntent=new Intent(this,DerdimizNeActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.bilimselbiri) {
            Intent ıntent=new Intent(this,BilimselBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.sosyalbiri) {
            Intent ıntent=new Intent(this,SosyalBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.kulturelbiri) {
            Intent ıntent=new Intent(this,KulturelBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.dusunenbiri) {
            Intent ıntent=new Intent(this,DusunenBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        else if (id == R.id.com) {
            Intent ıntent=new Intent(this,İletisimActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        else if (id == R.id.team) {
            Intent ıntent=new Intent(this,EkipActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        else if(id==R.id.facebook){
           Uri webpage=Uri.parse("https://www.facebook.com/kreatif6/");
           Intent webIntent=new Intent(Intent.ACTION_VIEW,webpage);
           this.startActivity(webIntent);
        }
        else if(id==R.id.instagram){
            Uri webpage=Uri.parse("https://www.instagram.com/kreatifbiri/");
            Intent webIntent=new Intent(Intent.ACTION_VIEW,webpage);
            this.startActivity(webIntent);
        }
        else if(id==R.id.twitter){
            Uri webpage = Uri.parse("https://twitter.com/kreatifbiri");
            Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
            this.startActivity(webIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private void bilimsel() {
        wordpressServices.categoryBilimsel("1","10","344","true").enqueue(new Callback<List<Post>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    ListData.bilimselList=response.body();
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }
    private void dusunen() {
        wordpressServices.categoryBilimsel("1","10","145","true").enqueue(new Callback<List<Post>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    ListData.dusunenList=response.body();
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }
    private void kulturel() {
        wordpressServices.categoryBilimsel("1","10","115","true").enqueue(new Callback<List<Post>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    ListData.kulturelList=response.body();
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }
    private void sosyal() {
        wordpressServices.categoryBilimsel("1","10","22","true").enqueue(new Callback<List<Post>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    ListData.sosyalList=response.body();
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }
    private void search() {
        wordpressServices.getAllPost("1","10","true").enqueue(new Callback<List<Post>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    ListData.searchList=response.body();
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }

}
