package app.kreatifbiri.com.kreatifbiricom.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.text.method.LinkMovementMethod;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.apı.GMailSender;
import butterknife.BindView;
import butterknife.ButterKnife;

public class EkipActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawer;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.age)
    EditText age;
    @BindView(R.id.blog)
    EditText blog;
    @BindView(R.id.desc)
    EditText desc;
    @BindView(R.id.editor)
    CheckBox editor;
    @BindView(R.id.web)
    CheckBox web;
    @BindView(R.id.android)
    CheckBox android;
    @BindView(R.id.sosyal)
    CheckBox sosyal;
    @BindView(R.id.diger)
    CheckBox diger;
    @BindView(R.id.digerText)
    EditText digerText;
    @BindView(R.id.ekipText)
    TextView ekipText;
    @BindView(R.id.pozisyon)
    TextView pozisyonText;
    private Typeface typeface;

    String pozisyon = null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ekip);
        ButterKnife.bind(this);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        typeface = Typeface.createFromAsset(getAssets(), "fonts/Raleway-Thin.ttf");
        ekipText.setTypeface(typeface);
        pozisyonText.setTypeface(typeface);

    }

    public void onSend(View v){
        if(name.getText().toString()!="" && email.getText().toString()!="" && age.getText().toString()!=""&&
                blog.getText().toString()!="" && desc.getText().toString()!="" || editor.isChecked() || web.isChecked()
                || android.isChecked() || sosyal.isChecked() || diger.isChecked()){
            if(editor.isChecked()){
                pozisyon += editor.getText().toString()+"\t";
            }
            else if(web.isChecked()){
                pozisyon += web.getText().toString()+"\t";
            }
            else if(android.isChecked()){
                pozisyon += android.getText().toString()+"\t";
            }
            else if(diger.isChecked()){
                pozisyon += digerText.getText().toString();
            }
            new SendMailTask().execute();
        }
        else{
            Toast.makeText(this,"Lütfen boş bıraktığınız yerleri doldurunuz.",Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent ıntent=new Intent(this,MainPageActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_backin,R.anim.anim_backout);
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if(id==R.id.kreatifbiri){
            Intent ıntent=new Intent(this,MainPageActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        if (id == R.id.whoare) {
            Intent ıntent=new Intent(this,BizKimizActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.whatsproblem) {
            Intent ıntent=new Intent(this,DerdimizNeActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.bilimselbiri) {
            Intent ıntent=new Intent(this,BilimselBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.sosyalbiri) {
            Intent ıntent=new Intent(this,SosyalBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.kulturelbiri) {
            Intent ıntent=new Intent(this,KulturelBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        } else if (id == R.id.dusunenbiri) {
            Intent ıntent=new Intent(this,DusunenBiriActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        else if (id == R.id.com) {
            Intent ıntent=new Intent(this,İletisimActivity.class);
            this.startActivity(ıntent);
            overridePendingTransition(R.anim.anim_in, R.anim.anim_out);
        }
        else if(id==R.id.facebook){
            Uri webpage=Uri.parse("https://www.facebook.com/kreatif6/");
            Intent webIntent=new Intent(Intent.ACTION_VIEW,webpage);
            this.startActivity(webIntent);
        }
        else if(id==R.id.instagram){
            Uri webpage=Uri.parse("https://www.instagram.com/kreatifbiri/");
            Intent webIntent=new Intent(Intent.ACTION_VIEW,webpage);
            this.startActivity(webIntent);
        }
        else if(id==R.id.twitter){
            Uri webpage = Uri.parse("https://twitter.com/kreatifbiri");
            Intent webIntent = new Intent(Intent.ACTION_VIEW, webpage);
            this.startActivity(webIntent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public class SendMailTask extends AsyncTask<Void,Void,Void> {

        ProgressDialog dialog;//dialog nesnesini tanımladık
        @Override
        protected  void onPreExecute()//Verilerin çekilme esnasında proggres dialog çalıştırır.
        {
            super.onPreExecute();
            dialog=new ProgressDialog(EkipActivity.this);
            dialog.setMessage("Mesajınız Gönderiliyor...");
            dialog.setIndeterminate(false);
            dialog.show();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                GMailSender sender = new GMailSender("kreatifbiri@gmail.com",
                        "75svrz11");
                sender.sendMail("Ekip Başvurusu", "Ad Soyad: "+name.getText().toString()+"\n"+
                                "eposta :"+email.getText().toString() +"\n"+
                                "Yaş :"+age.getText().toString()+"\n"+
                                "Blog :"+blog.getText().toString()+"\n"+
                                "Kendinden Bahsetmesi :"+desc.getText().toString()+"\n"+
                                "Pozisyon :"+pozisyon,
                        "kreatifbiri@gmail.com", "kreatifbiri@gmail.com");
            } catch (Exception e) {

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void avoid)//Arka plan işlemleri bittikten sonra başlık yazdırılır.
        {
            dialog.dismiss();
        }
    }
}
