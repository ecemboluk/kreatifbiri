package app.kreatifbiri.com.kreatifbiricom.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.activity.ContentActivity;
import app.kreatifbiri.com.kreatifbiricom.model.Author_;
import app.kreatifbiri.com.kreatifbiricom.model.Post;
import app.kreatifbiri.com.kreatifbiricom.model.WpFeaturedmedium_;
import app.kreatifbiri.com.kreatifbiricom.model.WpTerm_;

public class Adapter extends RecyclerView.Adapter<Adapter.MyAdapter> {
    private static final int CONTENT_TYPE =1 ;
    private static final int FLİPPER_TYPE=2;
    private static boolean isDots =true;
    private List<Post> postsList;
    private List<Post> flipperPostList=new ArrayList<>();
    private List<String> yazarList=new ArrayList<String>();
    private List<String> yorumList=new ArrayList<String>();
    private List<String> imageURL=new ArrayList<String>();
    private List<WpTerm_> wpTerm;
    private Activity activity;
    private ImageView dots [] =new ImageView[5];
    private WpFeaturedmedium_ wpFeaturedmedium;
    private Author_ author;
    private AdapterViewPager viewPagerAdapter;
    private Typeface typeface;

    public Adapter(List<Post> posts, Activity activity){
        this.activity=activity;
        this.postsList=posts;
    }
    @Override
    public MyAdapter onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=null;
        MyAdapter myAdapter = null;
        if(viewType==CONTENT_TYPE){
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_mainpage, parent, false);
            myAdapter=new MyAdapter(view);
        }
        else if(viewType==FLİPPER_TYPE) {
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_flipper, parent, false);
            isDots =true;
            myAdapter = new MyAdapter(view);
        }
        return myAdapter;
    }

    @Override
    public void onBindViewHolder(MyAdapter holder, final int position) {
        if(position==0){
           viewPagerAdapter=new AdapterViewPager(activity,flipperPostList);
           holder.viewPager.setAdapter(viewPagerAdapter);
           if(isDots){
               for(int i = 0; i < 5; i++){
                   dots[i] = new ImageView(activity);
                   dots[i].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.nonactive_dot));
                   LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                   params.setMargins(8, 0, 8, 0);
                   holder.sliderDotspanel.addView(dots[i], params);

               }
               dots[0].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.active_dot));
               isDots=false;
           }
           holder.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
               @Override
               public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

               }

               @Override
               public void onPageSelected(int position) {
                   for(int i = 0; i< 5; i++){
                       dots[i].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.nonactive_dot));
                   }
                   dots[position].setImageDrawable(ContextCompat.getDrawable(activity, R.drawable.active_dot));

               }
               @Override
               public void onPageScrollStateChanged(int state) {

               }
           });
        }
        else{
            wpTerm=postsList.get(position-1).getEmbedded().getWpTerm().get(0);
            wpFeaturedmedium=postsList.get(position-1).getEmbedded().getWpFeaturedmedia().get(0);
            author=postsList.get(position-1).getEmbedded().getAuthor().get(0);
            typeface = Typeface.createFromAsset(activity.getAssets(), "fonts/Raleway-Thin.ttf");
            holder.contentName.setTypeface(typeface);
            holder.kategori.setTypeface(typeface);
            holder.contentName.setText(postsList.get(position-1).getTitle().getRendered().replace("&#8217;","'")
            .replace("&#8220;","'").replace("&#8221","'"));
            holder.kategori.setText(wpTerm.get(0).getName());
            final String cat=wpTerm.get(0).getName();
            Glide.with(activity).load(wpFeaturedmedium.getSourceUrl()).into(holder.contentImage);
            holder.cv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent ıntent=new Intent(activity, ContentActivity.class);
                    if(postsList.get(position-1).getEmbedded().getReplies()==null){
                        ıntent.putExtra("yorum","no");
                    }
                    else{
                        ıntent.putExtra("yorum","yes");
                        int size=postsList.get(position-1).getEmbedded().getReplies().get(0).size();
                        for(int i=size-1; i>=0; i--){
                            yazarList.add(postsList.get(position-1).getEmbedded().getReplies().get(0).get(i).getAuthorName());
                            yorumList.add(postsList.get(position-1).getEmbedded().getReplies().get(0).get(i).getContent().getRendered());
                            imageURL.add(postsList.get(position-1).getEmbedded().getReplies().get(0).get(i).getAvatarUrls().get96());
                        }
                        ıntent.putStringArrayListExtra("commentsA", (ArrayList<String>) yazarList);
                        ıntent.putStringArrayListExtra("comments", (ArrayList<String>) yorumList);
                        ıntent.putStringArrayListExtra("images", (ArrayList<String>) imageURL);
                    }
                    ıntent.putExtra("link",postsList.get(position-1).getLink());
                    ıntent.putExtra("kategori",cat);
                    ıntent.putExtra("image",postsList.get(position-1).getEmbedded().getWpFeaturedmedia().get(0).getSourceUrl());
                    ıntent.putExtra("content",postsList.get(position-1).getContent().getRendered());
                    ıntent.putExtra("yazar",postsList.get(position-1).getEmbedded().getAuthor().get(0).getName());
                    ıntent.putExtra("desc",postsList.get(position-1).getEmbedded().getAuthor().get(0).getDescription());
                    ıntent.putExtra("yazarPhoto",postsList.get(position-1).getEmbedded().getAuthor().get(0).getAvatarUrls().get96());
                    ıntent.putExtra("id",postsList.get(position-1).getId());
                    ıntent.putExtra("title",postsList.get(position-1).getTitle().getRendered().replace("&#8217;","'"));
                    activity.startActivity(ıntent);
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return postsList.size();
    }
    @Override
    public int getItemViewType(int position)
    {
        if(position==0){
            return FLİPPER_TYPE;
        }
        else{
            return CONTENT_TYPE;
        }
    }
    public void add(List<Post>posts){
        int size=postsList.size();
        postsList.addAll(posts);
        notifyItemRangeInserted(size, postsList.size());
    }
    public void flipperAdd(List<Post> posts){
        for(int i=0; i<5; i++){
            flipperPostList.add(i,posts.get(i));
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    public static class MyAdapter extends RecyclerView.ViewHolder {

        CardView cv;
        TextView contentName;
        TextView kategori;
        ImageView contentImage;
        ViewPager viewPager;
        LinearLayout sliderDotspanel;

        public MyAdapter(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.cvMainpage);
            contentName = (TextView)itemView.findViewById(R.id.icerik_adi);
            kategori = (TextView) itemView.findViewById(R.id.cat);
            contentImage=(ImageView)itemView.findViewById(R.id.icerik_foto);
            viewPager=(ViewPager) itemView.findViewById(R.id.viewPager);
            sliderDotspanel=(LinearLayout)itemView.findViewById(R.id.SliderDots);
        }
    }
}
