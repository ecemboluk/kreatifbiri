package app.kreatifbiri.com.kreatifbiricom.activity;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.ValueAnimator;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.onesignal.OneSignal;

import java.util.ArrayList;
import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.adapter.Adapter;
import app.kreatifbiri.com.kreatifbiricom.adapter.AdapterSearch;
import app.kreatifbiri.com.kreatifbiricom.apı.ApiUtils;
import app.kreatifbiri.com.kreatifbiricom.model.ListData;
import app.kreatifbiri.com.kreatifbiricom.model.Post;
import app.kreatifbiri.com.kreatifbiricom.ınterface.WordpressServices;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchActivity extends AppCompatActivity {

    @BindView(R.id.rMainpage)
    RecyclerView rv;

    private AdapterSearch adapterSearch;
    private String query;
    private Context context;
    private WordpressServices wordpressServices;
    private List<Post> postList;
    private int page=1,previousTotal=0,visibleThreshold=12;
    private LinearLayoutManager llm;
    private Intent intent;
    private boolean loading = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        intent=this.getIntent();
        context=this;
        query=intent.getStringExtra("query");
        this.setTitle("Arama Sonucu: "+query);
        wordpressServices= ApiUtils.getWordpressServices();
        scrollReycleView();
    }

    private void scrollReycleView(){
        adapterSearch=new AdapterSearch(new ArrayList<Post>(),this);
        adapterSearch.filter(query,ListData.searchList);
        llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        rv.setHasFixedSize(true);
        rv.setItemAnimator(new DefaultItemAnimator());
        rv.setAdapter(adapterSearch);
        wordpressServices= ApiUtils.getWordpressServices();
        rv.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = rv.getChildCount();
                int totalItemCount = llm.getItemCount();
                int firstVisibleItem = llm.findFirstVisibleItemPosition();
                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount)
                        <= (firstVisibleItem + visibleThreshold)) {
                    page++;
                    load(page);
                    loading = true;
                }
                else{
                    page++;
                    load(page);
                    loading = true;
                }
            }
        });
    }
    private void load(int page) {
        wordpressServices.getAllPost(String.valueOf(page),"10","true").enqueue(new Callback<List<Post>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    postList=response.body();
                    adapterSearch.filter(query,postList);
                    adapterSearch.notifyDataSetChanged();
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }
}
