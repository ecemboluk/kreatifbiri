package app.kreatifbiri.com.kreatifbiricom.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import java.util.ArrayList;
import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.apı.ApiUtils;
import app.kreatifbiri.com.kreatifbiricom.model.ListData;
import app.kreatifbiri.com.kreatifbiricom.model.Post;
import app.kreatifbiri.com.kreatifbiricom.ınterface.WordpressServices;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SplashScreenActivity extends AppCompatActivity {

    private Window window;
    private WordpressServices wordpressServices;
    private List<Post> titleList;

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        window=getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
        titleList=new ArrayList<Post>();
        wordpressServices= ApiUtils.getWordpressServices();
        loadContent();
    }

    private void loadContent() {
        wordpressServices.getAllPost("1","10","true").enqueue(new Callback<List<Post>>() {
            @RequiresApi(api = Build.VERSION_CODES.KITKAT)
            @Override
            public void onResponse(Call<List<Post>> call, Response<List<Post>> response) {
                if(response.isSuccessful()){
                    titleList=response.body();
                    ListData.postList=titleList;
                    Intent i=new Intent(SplashScreenActivity.this,MainPageActivity.class);
                    startActivity(i);
                }
            }
            @Override
            public void onFailure(Call<List<Post>> call, Throwable t) {

            }
        });
    }
}
