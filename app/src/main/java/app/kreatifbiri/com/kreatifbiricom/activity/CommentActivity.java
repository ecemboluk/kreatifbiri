package app.kreatifbiri.com.kreatifbiricom.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.adapter.AdapterComments;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CommentActivity extends AppCompatActivity {

    private List<String > yorumC,yazarC,imageC;
    private Intent intent;
    private AdapterComments adapterComments;
    private LinearLayoutManager llm;

    @BindView(R.id.rvComments)
    RecyclerView recyclerView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        intent=this.getIntent();
        yorumC=intent.getStringArrayListExtra("yorum");
        yazarC=intent.getStringArrayListExtra("yazarC");
        imageC=intent.getStringArrayListExtra("image");
        adapterComments = new AdapterComments(yorumC,yazarC,imageC,this);
        llm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(llm);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapterComments);
    }

    public void onClickComments(View v){
        Intent intent=new Intent(this,SendCommentActivity.class);
        this.startActivity(intent);
    }
}
