package app.kreatifbiri.com.kreatifbiricom.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;


import java.util.List;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.adapter.AdapterComments;
import app.kreatifbiri.com.kreatifbiricom.apı.ApiUtils;
import app.kreatifbiri.com.kreatifbiricom.model.Post;
import app.kreatifbiri.com.kreatifbiricom.ınterface.WordpressServices;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ContentActivity extends AppCompatActivity {


    @BindView(R.id.yazar)
    TextView yazar;
    @BindView(R.id.description)
    TextView description;
    @BindView(R.id.yazarname)
    TextView yazaralt;
    @BindView(R.id.titleContent)
    TextView title;
    @BindView(R.id.kategori)
    TextView cat;
    @BindView(R.id.imageView2)
    ImageView image;
    @BindView(R.id.yazarPhoto)
    ImageView imageYazar;
    @BindView(R.id.content)
    WebView webView;
    @BindView(R.id.yorumButton)
    Button yorumButton;

    private Activity activity;
    private Intent ıntent;
    private InterstitialAd mInterstitialAd;
    private Window window;
    private String content, yorumString,link;
    private List<Post> posts;

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content);
        ButterKnife.bind(this);
        barColor();
        contentView();
    }

    //postun gösterilmesi
    public void contentView(){
        activity=this;
        webView = (WebView) findViewById(R.id.content);
        ıntent = this.getIntent();
        cat.setText(ıntent.getStringExtra("kategori"));
        yazaralt.setText(ıntent.getStringExtra("yazar"));
        link=ıntent.getStringExtra("link");
        content = ıntent.getStringExtra("content");
        content = content.replace("\\", "");
        content = content.replace("src=\"data:image/gif;base64,R0lGODlhAQABAAAAACH5BAEKAAEALAAAAAABAAEAAAICTAEAOw==\"", "");
        content = content.replace("data-layzr", "src");
        content=content.replace("width=", "width=320px");
        content=content.replace("height=", "height=200px");
        content=content.replace("<p>","<p color=#353638>");
        content =content.replace("&#8217;","'")
                .replace("&#8220;","'").replace("&#8221;","'");
        Glide.with(this).load(ıntent.getStringExtra("image")).into(image);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.loadDataWithBaseURL(null, content, "text/html", null, null);
        Glide.with(this).load(ıntent.getStringExtra("yazarPhoto")).into(imageYazar);
        yazar.setText(ıntent.getStringExtra("yazar"));
        description.setText(ıntent.getStringExtra("desc"));
        title.setText(ıntent.getStringExtra("title").replace("&#8217;","'")
                .replace("&#8220;","'").replace("&#8221;","'"));
        yorumString=ıntent.getStringExtra("yorum");
        if(yorumString.equals("no")){
            yorumButton.setText("İLK YORUMU YAP");
        }
        else{
            yorumButton.setText("YORUMLARI GÖR ("+ıntent.getStringArrayListExtra("comments").size()+")");
        }
        webView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return (event.getAction()==MotionEvent.ACTION_MOVE);
            }
        });
    }

    public void onClickComment(View v){
        if(yorumString.equals("no")){
            Intent intent=new Intent(this, SendCommentActivity.class);
            intent.putExtra("idC",ıntent.getIntExtra("id",0));
            this.startActivity(intent);
        }
        else{
            Intent intent=new Intent(this, CommentActivity.class);
            intent.putStringArrayListExtra("yorum",ıntent.getStringArrayListExtra("comments"));
            intent.putStringArrayListExtra("yazarC",ıntent.getStringArrayListExtra("commentsA"));
            intent.putStringArrayListExtra("image",ıntent.getStringArrayListExtra("images"));
            this.startActivity(intent);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void barColor(){
        window=getWindow();
        window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_MODE_CHANGED);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            window.setStatusBarColor(getResources().getColor(R.color.colorPrimaryDark));
        }
    }

    @Override
    public void onBackPressed() {
        Intent ıntent=new Intent(this,MainPageActivity.class);
        overridePendingTransition(R.anim.anim_backin,R.anim.anim_backout);
        this.startActivity(ıntent);
        super.onBackPressed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.menu_share, menu);
        MenuItem shareItem = menu.findItem(R.id.menu_search);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.share){
            Intent share = new Intent(Intent.ACTION_SEND);
            share.setType("text/plain");
            share.putExtra(Intent.EXTRA_TEXT, link);
            startActivity(Intent.createChooser(share, "Bilgiyi paylaş !! "));
        }
      return true;
    }


}
