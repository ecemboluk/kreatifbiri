package app.kreatifbiri.com.kreatifbiricom.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reply_ {

    @SerializedName("author_name")
    @Expose
    private String authorName;
    @SerializedName("content")
    @Expose
    private Content_ content;
    @SerializedName("author_avatar_urls")
    @Expose
    private AvatarUrls avatarUrls;


    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public Content_ getContent() {
        return content;
    }

    public void setContent(Content_ content) {
        this.content = content;
    }

    public AvatarUrls getAvatarUrls() {
        return avatarUrls;
    }

    public void setAvatarUrls(AvatarUrls avatarUrls) {
        this.avatarUrls = avatarUrls;
    }
}

