package app.kreatifbiri.com.kreatifbiricom.model;

import android.app.Application;

import java.util.List;

public class ListData extends Application {
    public static List<Post> postList;
    public static List<Post> bilimselList;
    public static List<Post> kulturelList;
    public static List<Post> dusunenList;
    public static List<Post> sosyalList;
    public static List<Post> searchList;
    private static ListData singleton;
    public static ListData getInstance() {
        return singleton;
    }
    @Override
    public void onCreate() {
        super.onCreate();
        singleton = this;
    }
}
