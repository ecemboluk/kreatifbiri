package app.kreatifbiri.com.kreatifbiricom.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;

import app.kreatifbiri.com.kreatifbiricom.R;
import app.kreatifbiri.com.kreatifbiricom.apı.ApiUtils;
import app.kreatifbiri.com.kreatifbiricom.model.CommentPost;
import app.kreatifbiri.com.kreatifbiricom.ınterface.WordpressServices;
import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendCommentActivity extends AppCompatActivity {

    @BindView(R.id.sendcomment)
    EditText comments;
    @BindView(R.id.name)
    EditText name;
    @BindView(R.id.email)
    EditText email;
    private Intent intent;
    private Activity activity;
    private WordpressServices wordpressServices;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        intent=this.getIntent();
        wordpressServices= ApiUtils.getWordpressServices();
        activity=this;
        setContentView(R.layout.activity_send_comment);
        ButterKnife.bind(this);
    }

    //yorum bilgilerini alır.

    public void onClik(View v){
        String authorName, authorEmail, content, postId;
        authorName=name.getText().toString();
        authorName.replaceAll(" ","%20");
        authorEmail=email.getText().toString().trim();
        content=comments.getText().toString();
        content.replaceAll(" ","%20");
        postId=String.valueOf(intent.getIntExtra("idC",0));
        if(!TextUtils.isEmpty(authorName) && !TextUtils.isEmpty(authorEmail) && !TextUtils.isEmpty(content) && !TextUtils.isEmpty(postId)) {

            sendPost(authorName,authorEmail,content,postId);
        }
        else{
            AlertDialog alertDialog = new AlertDialog.Builder(this)
                    .setTitle("Uyarı")
                    .setMessage("Boş bıraktığınız alanları lütfen doldurunuz. :)")
                    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    })
                    .show();

        }
    }

    //post ile gönderir.

    public void sendPost(String authorName, String authorEmail, String content, String postId) {
        wordpressServices.sendComment(authorName,authorEmail,content,postId).enqueue(new Callback<CommentPost>() {
            @Override
            public void onResponse(Call<CommentPost> call, Response<CommentPost> response) {
                if(response.isSuccessful()) {

                }
                AlertDialog alertDialog = new AlertDialog.Builder(activity)
                        .setTitle("Yorum")
                        .setMessage("Yorumunuz gönderildi. Onaylandıktan sonra yorumlar kısmında görüntülenecektir.")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .show();
            }

            @Override
            public void onFailure(Call<CommentPost> call, Throwable t) {

            }
        });
    }
}
